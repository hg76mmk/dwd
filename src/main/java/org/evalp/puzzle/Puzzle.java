package org.evalp.puzzle;

/**
 * @author Evgeny Pavlovsky
 */
public class Puzzle {

  static final String LETTERS =     "acdegilmnoprstuw";
  static final String HEX_LETTERS = "0123456789abcdef";
  static final long HASH = 945924806726376L;

  public static void main(String[] args) {
    final Puzzle puzzle = new Puzzle();
    puzzle.findSolution();
  }

  /**
   * Since our alphabet contains only 16 letters, we can convert any 9-letter word to
   * unique number between 0 and 68719476735 (0xFFFFFFFFF) and simply iterate by numbers.
   */
  private void findSolution() {
    final Converter converter = new Converter();
    final long start = 0x000_000_000L;
    final long end = 0xFFF_FFF_FFFL;

    long current = start;
    int blnCount = 0;

    while (current <= end) {
      long hash = converter.hashFromNumber(current);
      if (hash == HASH) {
        String original = converter.stringFromNumber(current);
        System.out.println("Solution: \t" + original);
        return;
      }
      if (current % 1_000_000_000 == 0 && current != 0) {
        System.out.println(++blnCount + " bln processed.");
      }
      current++;
    }
  }
}
