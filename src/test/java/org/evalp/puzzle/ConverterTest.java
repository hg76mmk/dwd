package org.evalp.puzzle;


import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.evalp.puzzle.Puzzle.HASH;

/**
 * @author Evgeny Pavlovsky
 */
public class ConverterTest {

  private Converter converter = new Converter();

  @Test
  public void convertsStringToHash() {
    long result = converter.hashFromString("leepadg");
    assertThat(result).isEqualTo(680131659347L);
  }

  @Test
  public void convertsNumberToHash() {
    final long result = converter.hashFromNumber(46061027363L);
    assertThat(result).isEqualTo(945924806726376L);
  }

  @Test
  public void convertsNumberToString() {
    final String result = converter.stringFromNumber(0xFAB013C42L);
    assertThat(result).isEqualTo("wpracesgd");
  }

  @Test
  public void solutionIsCorrect() {
    long result = converter.hashFromString("promenade");
    assertThat(result).isEqualTo(HASH);
  }
}
